<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTanggalTagihBerangkatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("berangkats", function(Blueprint $table){
            $table->dateTime("tanggal_tagih")->nullable()->after("keterangan_bayar");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("berangkats", function(Blueprint $table){
            $table->dropColumn(["tanggal_tagih"]);
        });
    }
}
