<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string("kode_pt",3);
            $table->date("tanggal")->nullable();
            $table->string("kode_lang",6);
            $table->decimal("jumlah",16,2)->nullable();
            $table->string("nomor_sj",15)->nullable();
            $table->string("nama")->nullable();
            $table->decimal("km",8,2)->nullable();
            $table->string("alt_tujuan",40)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
