<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBerangkatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berangkats', function (Blueprint $table) {
            $table->id();
            $table->string("kode_pt",3);
            $table->string("kode_sopir",6);
            $table->date("tanggal")->nullable();
            $table->string("nomor_sj",15)->nullable();
            $table->string("kode_lang",6)->nullable();
            $table->string("nama",30)->nullable();
            $table->decimal("jumlah",16,2)->nullable();
            $table->decimal("km",8,2)->nullable();
            $table->dateTime("tanggal_berangkat")->nullable();
            $table->string("alt_tujuan",40)->nullable();
            $table->decimal("jml_tujuan",2,0)->nullable();
            $table->dateTime("tanggal_datang")->nullable();
            $table->decimal("lama",6,0)->nullable();
            $table->decimal("bayar",16,2)->nullable();
            $table->string("keterangan_bayar",30)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berangkats');
    }
}
