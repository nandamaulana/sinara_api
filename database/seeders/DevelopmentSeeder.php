<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Sopir;
use App\Models\User;
use Illuminate\Database\Seeder;

class DevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataUser = [
            [
            "id" => 1,
            "kode_user" => "USR01",
            "nama" => "Nanda",
            "email" => "nandarm.sti@gmail.com",
            "password" => app("hash")->make("123456"),
            "telp" => "08123456789",
            "type" => "superadmin",
            "created_at" => null,
            "updated_at" => null,
            "deleted_at" => null
            ],
            [
            "id" => 2,
            "kode_user" => "USR02",
            "nama" => "Admin Sopir",
            "email" => "admin.sopir@gmail.com",
            "password" => app("hash")->make("123456"),
            "telp" => "08123456789",
            "type" => "admin_sopir",
            "created_at" => null,
            "updated_at" => null,
            "deleted_at" => null
            ],
            [
            "id" => 3,
            "kode_user" => "NAN01",
            "nama" => "Nanda R.M",
            "email" => "nandarm.sti1@gmail.com",
            "password" => app("hash")->make("123456"),
            "telp" => "083159730129",
            "type" => "superadmin",
            "created_at" => null,
            "updated_at" => null,
            "deleted_at" => null
            ]
        ];

        User::insert($dataUser);

        $dataSopir = [
            [
            "id" => 1,
            "kode_sopir" => "SOP01",
            "nama" => "Sopir 1 Edit",
            "alamat" => "Alamat 1",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 11:37:26",
            "updated_at" => "2022-01-25 11:31:02",
            "deleted_at" => "2022-01-25 11:31:02"
            ],
            [
            "id" => 2,
            "kode_sopir" => "SOP02",
            "nama" => "Sopir 2 Edit",
            "alamat" => "Alamat 2",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:45:03",
            "updated_at" => "2022-01-26 22:14:02",
            "deleted_at" => "2022-01-26 22:14:02"
            ],
            [
            "id" => 4,
            "kode_sopir" => "SOP04",
            "nama" => "Sopir 4",
            "alamat" => "Alamat 4",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:47:02",
            "updated_at" => "2022-01-24 15:47:02",
            "deleted_at" => null
            ],
            [
            "id" => 5,
            "kode_sopir" => "SOP05",
            "nama" => "Sopir 5",
            "alamat" => "Alamat 5",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:47:25",
            "updated_at" => "2022-01-24 15:47:25",
            "deleted_at" => null
            ],
            [
            "id" => 6,
            "kode_sopir" => "SOP06",
            "nama" => "Sopir 6",
            "alamat" => "Alamat 6",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:47:43",
            "updated_at" => "2022-01-24 15:47:43",
            "deleted_at" => null
            ],
            [
            "id" => 7,
            "kode_sopir" => "SOP07",
            "nama" => "Sopir 7",
            "alamat" => "Alamat 7",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:47:55",
            "updated_at" => "2022-01-24 15:47:55",
            "deleted_at" => null
            ],
            [
            "id" => 8,
            "kode_sopir" => "SOP08",
            "nama" => "Sopir 8",
            "alamat" => "Alamat 8",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:48:18",
            "updated_at" => "2022-01-24 15:48:18",
            "deleted_at" => null
            ],
            [
            "id" => 10,
            "kode_sopir" => "SOP09",
            "nama" => "Sopir 9",
            "alamat" => "Alamat 9",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:48:57",
            "updated_at" => "2022-01-24 15:48:57",
            "deleted_at" => null
            ],
            [
            "id" => 12,
            "kode_sopir" => "SOP10",
            "nama" => "Sopir 10",
            "alamat" => "Alamat 10",
            "telepon" => "083159730129",
            "created_at" => "2022-01-24 15:49:34",
            "updated_at" => "2022-01-24 15:49:34",
            "deleted_at" => null
            ],
            [
            "id" => 14,
            "kode_sopir" => "SOP11",
            "nama" => "Sopir 11",
            "alamat" => "Alamat 11",
            "telepon" => "083159730129",
            "created_at" => "2022-01-26 17:00:39",
            "updated_at" => "2022-01-26 17:00:39",
            "deleted_at" => null
            ]];

        Sopir::insert($dataSopir);

        $dataCustomer = [
            [
            "id" => 1,
            "kode_lang" => "LAN001",
            "nama" => "Nanda Langgan",
            "alamat" => "Alamat langgan 1",
            "telepon" => "083159730129",
            "created_at" => "2022-01-25 14:04:56",
            "updated_at" => "2022-01-25 14:04:59",
            "deleted_at" => null
            ],
            [
            "id" => 2,
            "kode_lang" => "LAN002",
            "nama" => "Nanda lan 2 edit",
            "alamat" => "alamat 2",
            "telepon" => "083159730129",
            "created_at" => "2022-01-25 14:35:47",
            "updated_at" => "2022-01-26 11:01:22",
            "deleted_at" => "2022-01-26 11:01:22"
            ]];

        Customer::insert($dataCustomer);
    }
}
