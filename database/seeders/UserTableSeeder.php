<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainUser = [
            [
                "kode_user" => "USR01",
                "nama" => "Nanda",
                "email" => "nandarm.sti@gmail.com",
                "password" => app("hash")->make("123456"),
                "telp" => "08123456789",
                "type" => "superadmin"
            ],
            [
                "kode_user" => "USR02",
                "nama" => "Admin Sopir",
                "email" => "admin.sopir@gmail.com",
                "password" => app("hash")->make("123456"),
                "telp" => "08123456789",
                "type" => "admin_sopir"
            ],
        ];

        User::insert($mainUser);
    }
}
