<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use tibonilab\Pdf\Pdf;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    $pdf = new Pdf();
    $html = '<html><body>'
			. '<p>Put your html here, or generate it with your favourite '
			. 'templating system.</p>'
			. '</body></html>';
	return $pdf->load($html, 'A4', 'portrait')->download("test");
});
$router->group(['prefix' => 'v1'], function () use ($router) {

   $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');

    $router->group(["middleware" => "auth"], function() use ($router){
        $router->get('profile', 'UserController@profile');

        $router->get('users/{email}', 'UserController@singleUser');
        $router->post('users/getall', 'UserController@getAll');
        $router->post('users/edit', 'UserController@edit');
        $router->post('users/detail', 'UserController@getDetail');
        $router->post('users/delete', 'UserController@delete');

        $router->post("sopir/tambah", "SopirController@tambah");
        $router->post("sopir/edit", "SopirController@edit");
        $router->post("sopir/getall", "SopirController@getall");
        $router->post("sopir/getdetail", "SopirController@getDetail");
        $router->post("sopir/delete", "SopirController@delete");

        $router->post("invoice/tambah", "InvoiceController@tambah");
        $router->post("invoice/edit", "InvoiceController@edit");
        $router->post("invoice/getall", "InvoiceController@getall");
        $router->post("invoice/getavailableinvoice", "InvoiceController@getAvailableInvoice");
        $router->post("invoice/getdetail", "InvoiceController@getDetail");
        $router->post("invoice/delete", "InvoiceController@delete");

        $router->post("customer/tambah", "CustomerController@tambah");
        $router->post("customer/edit", "CustomerController@edit");
        $router->post("customer/getall", "CustomerController@getall");
        $router->post("customer/getdetail", "CustomerController@getDetail");
        $router->post("customer/delete", "CustomerController@delete");

        $router->post("berangkat/keberangkatan", "BerangkatController@keberangkatan");
        $router->post("berangkat/edit", "BerangkatController@edit");
        $router->post("berangkat/getall", "BerangkatController@getall");
        $router->post("berangkat/getdetail", "BerangkatController@getDetail");
        $router->post("berangkat/delete", "BerangkatController@delete");
        $router->post("berangkat/getdatangfilter", "BerangkatController@getDatangFilter");
        $router->post("berangkat/getjamberangkatbysopirandtanggalberangkat", "BerangkatController@getJamBerangkatBySopirAndTanggalBerangkat");
        $router->post("berangkat/editkedatangan", "BerangkatController@editKedatangan");

        $router->post("laporan/getsopirreport", "LaporanController@getSopirReport");
    });
});
