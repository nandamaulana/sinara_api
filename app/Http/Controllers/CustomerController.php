<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Exception;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\String_;
use Stringable;

class CustomerController extends Controller
{

    public function tambah(Request $request)
    {
        $this->validate($request, [
            'kode_lang' => 'required|string|unique:customers|max:6',
            'nama' => 'required|string|max:30',
            'alamat' => 'string|max:40|nullable',
            'telepon' => 'string|max:20|nullable',
        ]);

        try {

            $data = new Customer();
            $data->kode_lang = $request->input("kode_lang");
            $data->nama = $request->input("nama");
            $data->alamat = $request->input("alamat");
            $data->telepon = $request->input("telepon");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'CREATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Tambah Customer gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'kode_lang' => 'required|string|max:6',
            'nama' => 'required|string|max:30',
            'alamat' => 'string|max:40|nullable',
            'telepon' => 'string|max:20|nullable',
        ]);
        try {
            $data = Customer::findOrFail($request->input("id"));
            $data->kode_lang = $request->input("kode_lang");
            $data->nama = $request->input("nama");
            $data->alamat = $request->input("alamat");
            $data->telepon = $request->input("telepon");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'UPDATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Edit Customer gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function getAll(Request $request)
    {
        try {
            $result = Customer::when($request->input("kode_lang"), function($query) use ($request){
                $query->orWhere("kode_lang", "like", "%".$request->input("kode_lang")."%");
            })->when($request->input("nama"), function($query) use ($request){
                $query->orWhere("nama", 'like', $this->wildcardChar($request->input("nama")));
            })->when($request->input("alamat"), function($query) use ($request){
                $query->orWhere("alamat", "like", $this->wildcardChar($request->input("alamat")));
            })->when($request->input("telepon"), function($query) use ($request){
                $query->orWhere("telepon", "%".$request->input("telepon")."%");
            })->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {

            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getDetail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $result = Customer::with(["invoices", "berangkats"])->where("id",$request->input("id"))->first();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $find = Customer::find($request->input("id"));
            if (!$find) {
                throw new Exception("Customer not found");
            }
            $result = $find->delete();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }
}
