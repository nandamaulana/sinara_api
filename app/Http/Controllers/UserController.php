<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
     /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
        return response()->json(['data' => Auth::user(), "message" => "success"], 200);
    }

    public function getAll(Request $request)
    {
        try {
            $result = User::when($request->input("kode_user"), function($query) use ($request){
                $query->orWhere("kode_user", "like", "%".$request->input("kode_user")."%");
            })->when($request->input("nama"), function($query) use ($request){
                $query->orWhere("nama", 'like', $this->wildcardChar($request->input("nama")));
            })->when($request->input("email"), function($query) use ($request){
                $query->orWhere("email", "like", "%".$request->input("email")."%");
            })->when($request->input("telp"), function($query) use ($request){
                $query->orWhere("telp", "like", "%".$request->input("telp")."%");
            })->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {

            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($email)
    {
        try {
            $user = User::where("email",$email)->first();

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $find = User::find($request->input("id"));
            if (!$find) {
                throw new Exception("User not found");
            }
            $result = $find->delete();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'kode_user' => 'required|string|max:5',
            'nama' => 'required|string|max:25',
            'telp' => 'required|string|max:20',
            'type' => 'required|string|in:admin_sopir,superadmin,owner',
        ]);
        try {
            $data = User::findOrFail($request->input("id"));
            if ($request->input("kode_user")) {
                $data->kode_user = $request->input("kode_user");
            }
            $data->nama = $request->input("nama");
            if ($request->input("email")) {
                $data->email = $request->input("email");
            }
            if ($request->input("password")) {
                $data->password = app('hash')->make($request->input("password"));
            }
            $data->telp = $request->input("telp");
            $data->type = $request->input("type");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'UPDATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Edit Customer gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function getDetail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $result = User::where("id",$request->input("id"))->first();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }
}
