<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'kode_user' => 'required|unique:users|string|max:5',
            'nama' => 'required|string|max:25',
            'email' => 'required|email|unique:users|max:40',
            'password' => 'required|confirmed|max:15',
            'telp' => 'required|string|max:20',
            'type' => 'required|string|in:admin_sopir,superadmin,owner',
        ]);

        try {

            $user = new User();
            $user->kode_user = $request->input("kode_user");
            $user->nama = $request->input("nama");
            $user->email = $request->input("email");
            $plainPassword = $request->input("password");
            $user->telp = $request->input("telp");
            $user->type = $request->input("type");
            $user->password = app('hash')->make($plainPassword);
            $user->save();

            //return successful response
            return response()->json(['data' => $user, 'message' => 'CREATED'], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!', "error" => $e->getMessage()], 409);
        }

    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
          //validate incoming request
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }
}
