<?php

namespace App\Http\Controllers;

use App\Models\Berangkat;
use Exception;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\String_;
use Stringable;

use function PHPUnit\Framework\isNull;

class BerangkatController extends Controller
{

    public function keberangkatan(Request $request)
    {
        $this->validate($request, [
            "data.*.kode_sopir" => "required|string|max:6",
            "data.*.tanggal" => "required|date_format:Y-m-d",
            "data.*.nomor_sj" => "required|string|max:15",
            "data.*.kode_lang" => "required|string|max:6",
            "data.*.nama" => "required|string|max:30",
            "data.*.jumlah" => "required",
            "data.*.tanggal_berangkat" => "required|date_format:Y-m-d H:i:s",
        ]);
        try {
            $collectionData = collect($request->get("data"))->map(function($item){
                if ($exist = Berangkat::where("nomor_sj", $item["nomor_sj"])->whereNull("tanggal_tagih")->first()) {
                    $exist->tanggal_tagih = $item["tanggal_berangkat"];
                    $exist->keterangan_bayar = "Belum lunas - Tagih ulang";
                    $exist->save();
                    $item["jumlah"] = ((int)$item["jumlah"]) - $exist->bayar;
                }
                $item["created_at"] = date("Y-m-d H:i:s");
                $item["updated_at"] = date("Y-m-d H:i:s");
                return $item;
            })->toArray();
            $result = Berangkat::insert($collectionData);

            return response()->json(['data' => $result, 'message' => 'CREATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Tambah Berangkat gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function editKedatangan(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            "tanggal_datang" => "required|date_format:Y-m-d H:i:s",
            "lama" => "required|string",
            "bayar" => "required",
            "keterangan_bayar" => "required|string"
        ]);

        try {
            $data = Berangkat::findOrFail($request->input("id"));
            $data->tanggal_datang = $request->input("tanggal_datang");
            $data->lama = $request->input("lama");
            $data->bayar = $request->input("bayar");
            $data->keterangan_bayar = $request->input("keterangan_bayar");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'UPDATED'], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Edit Berangkat gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            "kode_pt" => "required|string|max:3",
            "kode_sopir" => "required|string|max:6",
            "tanggal" => "required|date_format:Y-m-d",
            "nomor_sj" => "required|string|max:15",
            "kode_lang" => "required|string|max:6",
            "nama" => "required|string|max:30",
            "jumlah" => "required",
            "tanggal_berangkat" => "required|date_format:Y-m-d H:i:s",
        ]);
        try {
            $data = Berangkat::findOrFail($request->input("id"));
            $data->kode_pt = $request->input("kode_pt");
            $data->kode_sopir = $request->input("kode_sopir");
            $data->tanggal = $request->input("tanggal");
            $data->nomor_sj = $request->input("nomor_sj");
            $data->kode_lang = $request->input("kode_lang");
            $data->nama = $request->input("nama");
            $data->jumlah = $request->input("jumlah");
            $data->tanggal_berangkat = $request->input("tanggal_berangkat");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'Updated'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Edit Berangkat gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function getAll(Request $request)
    {
        try {
            $result = Berangkat::when($request->input("kode_lang"), function($query) use ($request){
                $query->orWhere("kode_lang", "like", "%".$request->input("kode_lang")."%");
            })->when($request->input("nama"), function($query) use ($request){
                $query->orWhere("nama", 'like', $this->wildcardChar($request->input("nama")));
            })->when($request->input("alamat"), function($query) use ($request){
                $query->orWhere("alamat", "like", $this->wildcardChar($request->input("alamat")));
            })->when($request->input("telepon"), function($query) use ($request){
                $query->orWhere("telepon", "like", "%".$request->input("telepon")."%");
            })->when($request->input("tanggal"), function($query) use ($request){
                $query->orWhere(DB::raw("DATE_FORMAT(tanggal, '%d-%m-%Y')"), "like", "%".$request->input("tanggal")."%");
            })->when($request->input("tanggal_berangkat"), function($query) use ($request){
                $query->orWhere(DB::raw("DATE_FORMAT(tanggal_berangkat, '%d-%m-%Y')"), "like", "%".$request->input("tanggal_berangkat")."%");
            })->when($request->input("jumlah"), function($query) use ($request){
                $query->orWhere("jumlah", "like", "%".$request->input("jumlah")."%");
            })->when($request->input("order"), function($query) use ($request){
                $order = $request->input("order");
                $query->orderBy(DB::raw($order["order_table"]), $order["order_sort"]);
            })->when($request->input("filter") != "Semua", function($query) use ($request){
                $query->where("tanggal_berangkat", ">=", DB::raw("DATE_ADD(CURDATE(), INTERVAL -3 DAY)"));
            })
            // ->when($request->input("timestamp"), function($query) use ($request){
            //     if($request->input("timestamp") != "NEW")
            //         $query->where("update_at", "<", $request->input("timestamp"));
            //     $query->limit(10);
            // })
            ->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getDatangFilter(Request $request)
    {
        try {
            $result = Berangkat::where("kode_sopir", 'like', $request->input("kode_sopir"))
            ->where(DB::raw("DATE_FORMAT(tanggal_berangkat, '%Y-%m-%d %H:%i:%s')"), "like", "%".$request->input("tanggal_berangkat")."%")
            ->where(DB::raw("IFNULL(bayar,0)"), "<", DB::raw("jumlah"))
            ->whereNull("tanggal_tagih")
            ->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getJamBerangkatBySopirAndTanggalBerangkat(Request $request)
    {
        try {
            $result = Berangkat::select(DB::raw('DISTINCT(DATE_FORMAT(tanggal_berangkat, "%H:%i")) jam '))
            ->where("kode_sopir","=",$request->input("kode_sopir"))
            ->where("tanggal_berangkat","LIKE","%".$request->input("tanggal_berangkat")."%")
            ->where(DB::raw("IFNULL(bayar,0)"), "<",DB::raw("jumlah"))
            ->whereNull("tanggal_tagih")
            ->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getDetail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $result = Berangkat::with(["invoices", "berangkats"])->where("id",$request->input("id"))->first();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $find = Berangkat::find($request->input("id"));
            if ($exist = Berangkat::where("nomor_sj", $find->nomor_sj)->where("tanggal_tagih", $find->tanggal_berangkat)->first()) {
                $exist->tanggal_tagih = null;
                $exist->keterangan_bayar = "Belum Lunas";
                $exist->save();
            }
            if (!$find) {
                throw new Exception("Berangkat not found");
            }
            $result = $find->delete();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }
}
