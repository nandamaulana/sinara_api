<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Sopir;
use Exception;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\String_;
use Stringable;

class InvoiceController extends Controller
{

    public function tambah(Request $request)
    {
        $this->validate($request, [
            'tanggal' => 'date_format:Y-m-d|nullable',
            'kode_lang' => 'required|string|max:6|nullable',
            'nomor_sj' => ['string',Rule::unique("invoices")->whereNull("deleted_at"),'max:15','nullable'],
            'nama' => 'string|nullable',
            'alt_tujuan' => 'string|max:40',
        ]);

        try {

            $data = new Invoice();
            $data->kode_pt = $request->input("kode_pt") ?: "";
            $data->tanggal = $request->input("tanggal");
            $data->kode_lang = $request->input("kode_lang");
            $data->jumlah = $request->input("jumlah");
            $data->nomor_sj = $request->input("nomor_sj");
            $data->nama = $request->input("nama");
            $data->km = $request->input("km");
            $data->alt_tujuan = $request->input("alt_tujuan");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'CREATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Tambah invoice gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'tanggal' => 'date_format:Y-m-d|nullable',
            'kode_lang' => 'required|string|max:6|nullable',
            'nomor_sj' => ["string", Rule::unique("invoices")->ignore($request->input("id")), "max:15", "nullable"],
            'nama' => 'string|nullable',
            'alt_tujuan' => 'string|max:40',
        ]);
        try {
            $data = Invoice::findOrFail($request->input("id"));
            $data->kode_pt = $request->input("kode_pt");
            $data->tanggal = $request->input("tanggal");
            $data->kode_lang = $request->input("kode_lang");
            $data->jumlah = $request->input("jumlah");
            $data->nomor_sj = $request->input("nomor_sj");
            $data->nama = $request->input("nama");
            $data->km = $request->input("km");
            $data->alt_tujuan = $request->input("alt_tujuan");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'UPDATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Edit invoice gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function getAll(Request $request)
    {
        try {
            DB::enableQueryLog();

            $result = Invoice::when($request->input("kode_pt"), function ($query) use ($request) {
                $query->orWhere("kode_pt", "like", "%" . $request->input("kode_pt") . "%");
            })->when($request->input("tanggal"), function ($query) use ($request) {
                $query->orWhere("tanggal", "like", $request->input("tanggal"));
            })->when($request->input("kode_lang"), function ($query) use ($request) {
                $query->orWhere("kode_lang", "like", "%" . $request->input("kode_lang") . "%");
            })->when($request->input("jumlah"), function ($query) use ($request) {
                $query->orWhere("jumlah", "like", "%" . $request->input("jumlah") . "%");
            })->when($request->input("nomor_sj"), function ($query) use ($request) {
                $query->orWhere("nomor_sj", "like", "%" . $request->input("nomor_sj") . "%");
            })->when($request->input("nama"), function ($query) use ($request) {
                $query->orWhere("nama", "like", $this->wildcardChar($request->input("nama")));
            })->when($request->input("km"), function ($query) use ($request) {
                $query->orWhere("km", "like", "%" . $request->input("km") . "%");
            })->when($request->input("alt_tujuan"), function ($query) use ($request) {
                $query->orWhere("alt_tujuan", "like", $this->wildcardChar($request->input("alt_tujuan")));
            })->with(["customer"])->get();
            // return response()->json(DB::getQueryLog());
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            Log::info($e->getMessage());
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getAvailableInvoice(Request $request)
    {
        try {
            DB::enableQueryLog();
            $result = Invoice::select(DB::raw("invoices.*,berangkats.jumlah, invoices.jumlah as jumlah_invoice, berangkats.keterangan_bayar,berangkats.bayar,berangkats.tanggal_berangkat berangkat_sebelumnya"))
                ->leftJoin("berangkats","invoices.nomor_sj", "=", DB::raw("berangkats.nomor_sj AND berangkats.deleted_at IS NULL"))
                ->whereNull("berangkats.tanggal_berangkat")
                ->orWhere(function($query){
                    $query->whereNotNull("berangkats.tanggal_datang")
                    ->where(DB::raw("IFNULL(berangkats.bayar,0)"), "<", DB::raw("invoices.jumlah"))
                    ->whereNull("berangkats.tanggal_tagih");
                })
                ->orderBy("berangkats.jumlah")
                ->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            Log::info($e->getMessage());
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getDetail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $result = Invoice::with(["customer"])->where("id", $request->input("id"))->first();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {

            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $find = Invoice::find($request->input("id"));
            if (!$find) {
                throw new Exception("invoice not found");
            }
            $result = $find->delete();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }
}
