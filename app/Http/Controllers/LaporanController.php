<?php

namespace App\Http\Controllers;

use App\Models\Berangkat;
use App\Models\Sopir;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Locale;
use NumberFormatter;
use tibonilab\Pdf\Pdf;

class LaporanController extends Controller
{

    public function getSopirReport(Request $request)
    {
        $pdf = new Pdf();
        $berangkat = Sopir::with(["berangkats" => function($berangkat) use($request){
            return $berangkat->whereBetween("tanggal_berangkat", [$request->input("tanggal_awal"), $request->input("tanggal_akhir")]);
        }])->where("kode_sopir",$request->input("kode_sopir"))
            ->get()->first();
        $html = '<html><head>';
        $html .= "<style>.title {font-size: 24px;font-weight: bold;}.center-horizontal{width:100%;text-align:center;}.horizontal-line{width:100%;height:2px;background-color:black;}.summary{margin-top:8px;}.table-berangkat{margin-top:8px;} .table-berangkat , .table-berangkat td{border: 1px solid;border-collapse: collapse;padding:3px;vertical-align: top;text-align: left;}</style></head><body>";
        $html .= '<p class="title center-horizontal">Laporan Keberangkatan Sopir</p></br><div class="horizontal-line"></div>';
        $html .= "<div><table class='data-sopir' >
            <tr>
                <td>Data Sopir</td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>: ".Carbon::parse($request->input("tanggal_awal"))->translatedFormat("d F Y")." ~ ".Carbon::parse($request->input("tanggal_akhir"))->translatedFormat("d F Y")."</td>
            </tr>
            <tr>
                <td>Kode Sopir</td>
                <td>: ".$berangkat->kode_sopir."</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>: ".$berangkat->nama."</td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>: ".$berangkat->telepon."</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>: ".$berangkat->alamat."</td>
            </tr>
            </table></div>";

        $counted = $berangkat->berangkats->countBy(function($item){
            return $item->keterangan_bayar;
        });

        $html .= "<div class='summary'>Total data: ".$berangkat->berangkats->count()." (Berangkat = ".($counted[""] ?? 0) .", Lunas = ".($counted["Lunas"] ?? 0).", Belum lunas = ".($counted["Belum Lunas"] ?? 0) .")</div>";
        $html .= "<table class='table-berangkat' style='width:100%;'>";
        $html .= "
                <tr>
                    <td style='width:1%;'>No.</td>
                    <td>Invoice</td>
                    <td>Keberangkatan</td>
                    <td>Kedatangan</td>
                </tr>";
        foreach ($berangkat->berangkats as $key => $value) {
            $no = $key+1;
            $html .= "
                <tr>
                    <td>
                        $no
                    </td>
                    <td>
                        Tanggal Invoice: ".Carbon::parse($value->tanggal)->translatedFormat("d F Y")."<br>
                        No Invoice: ". $value->nomor_sj ."<br>
                        Nilai Tagihan: Rp. ".number_format($value->jumlah)."
                    </td>
                    <td>
                        Tanggal Berangkat: ".Carbon::parse($value->tanggal_berangkat)->translatedFormat("d F Y")."<br>
                        Jam Berangkat: ". Carbon::parse($value->tanggal_berangkat)->translatedFormat("H:i:s")."<br>
                        Alamat Tujuan: ". $value->alt_tujuan ."<br>
                        Perkiraan KM: ". $value->km ."<br>
                        Jumlah tujuan: ".$value->jml_tujuan."
                    </td>
                    <td>
                        Tgl Kembali: ". ($value->tanggal_datang ? Carbon::parse($value->tanggal_datang)->translatedFormat("d F Y") : "-")."<br>
                        Jam Kembali: ". ($value->tanggal_datang ? Carbon::parse($value->tanggal_datang)->translatedFormat("H:i:s") : "-")."<br>
                        Lama: ".($value->lama ? $value->ReadableLama : "-")."<br>
                        Terbayar: Rp. ".($value->bayar ? number_format($value->bayar) : "-")."<br>
                        Selisih: Rp. ".($value->bayar ? number_format($value->jumlah - $value->bayar) : "-")."<br>
                        Keterangan: ".($value->keterangan_bayar ?? "-")."
                        ".($value->tanggal_tagih ? "<br>Tanggal tagih: ".Carbon::parse($value->tanggal_datang)->translatedFormat("d F Y H:i") : "")."
                    </td>
                </tr>";
        }

        $html .= '</table></body></html>';
        return $pdf->load($html, 'A4', 'portrait')->download("test");
    }


}
