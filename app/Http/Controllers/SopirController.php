<?php

namespace App\Http\Controllers;

use App\Models\Sopir;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\String_;
use Stringable;

class SopirController extends Controller
{

    public function tambah(Request $request)
    {
        $this->validate($request, [
            'kode_sopir' => ['required',
                            'string',
                            Rule::unique("sopirs")->whereNull("deleted_at"),
                        'max:6'],
            'nama' => 'required|string|max:25',
            'alamat' => 'required|string|max:40',
            'telepon' => 'required|string|max:20',
        ]);

        try {

            $data = new Sopir();
            $data->kode_sopir = $request->input("kode_sopir");
            $data->nama = $request->input("nama");
            $data->alamat = $request->input("alamat");
            $data->telepon = $request->input("telepon");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'CREATED'], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Tambah sopir gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'kode_sopir' => 'required|string|unique:sopirs|max:6',
            'nama' => 'required|string|max:25',
            'alamat' => 'required|string|max:40',
            'telepon' => 'required|string|max:20',
        ]);
        try {
            $data = Sopir::findOrFail($request->input("id"));
            $data->kode_sopir = $request->input("kode_sopir");
            $data->nama = $request->input("nama");
            $data->alamat = $request->input("alamat");
            $data->telepon = $request->input("telepon");
            $data->save();

            return response()->json(['data' => $data, 'message' => 'UPDATED'], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Edit sopir gagal!', "error" => $e->getMessage()], 409);
        }
    }

    public function getAll(Request $request)
    {
        try {
            $result = Sopir::when($request->input("kode_sopir"), function($query) use ($request){
                $query->orWhere("kode_sopir", "like", "%".$request->input("kode_sopir")."%");
            })->when($request->input("nama"), function($query) use ($request){
                $query->orWhere("nama","like", $this->wildcardChar($request->input("nama")));
            })->when($request->input("alamat"), function($query) use ($request){
                $query->orWhere("alamat","like", $this->wildcardChar($request->input("alamat")));
            })->when($request->input("telepon"), function($query) use ($request){
                $query->orWhere("telepon","like", "%".$request->input("telepon")."%");
            })->get();
            return response()->json(['data' => $result, 'message' => count($result) ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {

            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function getDetail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $result = Sopir::with(["berangkats"])->where("id",$request->input("id"))->first();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {

            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|string',
        ]);
        try {
            $result = Sopir::find($request->input("id"))->delete();
            return response()->json(['data' => $result, 'message' => $result ? 'Success' : "no data"], 200);
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Internal server error', "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 409);
        }
    }
}
