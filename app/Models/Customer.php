<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $fillable = [
        "kode_lang",
        "nama",
        "alamat",
        "telepon",
    ];

    public function invoices()
    {
        return $this->hasMany(Invoice::class, "kode_lang", "kode_lang");
    }

    public function berangkats()
    {
        return $this->hasMany(Berangkat::class, "kode_lang", "kode_lang");
    }
}
