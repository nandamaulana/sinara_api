<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sopir extends Model
{
    use SoftDeletes;
    protected $fillable = [
        "kode_sopir",
        "nama",
        "alamat",
        "telepon",
    ];

    public function berangkats()
    {
        return $this->hasMany(Berangkat::class, "kode_sopir", "kode_sopir");
    }
}
