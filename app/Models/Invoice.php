<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    protected $fillable = [
        "kode_pt",
        "tanggal",
        "kode_lang",
        "jumlah",
        "nomor_sj",
        "nama",
        "km",
        "alt_tujuan",
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, "kode_lang", "kode_lang");
    }

    public function berangkat()
    {
        return $this->hasOne(Berangkat::class, "nomor_sj", "nomor_sj");
    }
}
