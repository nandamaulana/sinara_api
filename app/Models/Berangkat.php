<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Berangkat extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $fillable = [
        "kode_pt",
        "kode_sopir",
        "tanggal",
        "nomor_sj",
        "kode_lang",
        "nama",
        "jumlah",
        "km",
        "tanggal_berangkat",
        "alt_tujuan",
        "jml_tujuan",
        "tanggal_datang",
        "lama",
        "bayar",
        "keterangan_bayar",
    ];

    public function sopir()
    {
        return $this->belongsTo(Sopir::class,"kode_sopir","kode_sopir");
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, "kode_lang", "kode_lang");
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, "nomor_sj", "nomor_sj");
    }

    public function getReadableLamaAttribute()
    {
        $result = "";
        if ($this->lama) {
            $hour = floor(((double) $this->lama) / 60);
            $minutes = ((double) $this->lama) % 60;
            $result = "$hour Jam $minutes Menit";
        }
        return $result;
    }

}
